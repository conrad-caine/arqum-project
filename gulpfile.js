var gulp = require('gulp'),
	jshint = require('gulp-jshint'),
	uglify = require('gulp-uglify'),
	stylus = require('gulp-stylus'),
	nib = require('nib'),
	connect = require('gulp-connect');

var path = {
	js: {
		src: __dirname + '/js/src/*.js',
		dest: __dirname + '/js/'
	},
	css: {
		src: __dirname + '/css/stylus/*.styl',
		dest: __dirname + '/css/'
	},
	html: __dirname + '/*.html'
}

gulp.task('html', function () {
	gulp.src(path.html)
		.pipe(connect.reload());
});

gulp.task('css', function () {
	gulp.src(path.css.dest + 'stylus/main.styl')
		.pipe(stylus({
			use: [nib()],
			compress: false,
			errors: true
		}))
		.pipe(gulp.dest(path.css.dest));

	gulp.src(path.css.dest + 'main.css')
		.pipe(connect.reload());
});

gulp.task('js', function () {
	gulp.src(path.js.src)
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(uglify())
		.pipe(gulp.dest(path.js.dest))
		.pipe(connect.reload());
});

gulp.task('connect', function () {
	connect.server({
		root: __dirname,
		port: 8000,
		livereload: true
	});
});

gulp.task('watch', function () {
	gulp.watch([path.html], ['html']);
	gulp.watch([path.css.src], ['css']);
	gulp.watch([path.js.src], ['js']);
});

gulp.task('build', ['html', 'css', 'js']);
gulp.task('default', ['build', 'connect', 'watch']);
