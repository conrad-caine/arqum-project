Arqum Project
=============

*Front-end template*

Getting started
---------------

	git clone git@bitbucket.org:conrad-caine/arqum-project.git
	cd arqum-project
	npm install
	bower install


Usage
-----

* Run `gulp`
* Build `gulp build`
