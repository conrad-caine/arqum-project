/*
 * Paginator - v1.0
 *
 * author: Acauã Montiel <contato@acauamontiel.com.br>
 *
 * Usage:
 * var newPaginator = new Paginator(int);
 */
var Paginator = function(length, circular) {
    var last = length - 1; // Last index
    circular = (typeof circular === 'undefined') ? true : circular;

    this.current = 0; // Current index starts at 0

    this.set = function(index) {
        // Sets the current index with the number entered in the index parameter if it is not greater than the last index
        this.current = (index <= last) ? index : last;
    };

    this.hasPrev = function() {
        // Returns whether the previous index
        return this.current !== 0;
    };

    this.hasNext = function() {
        // Returns whether the next index
        return this.current < last;
    };

    this.getPrev = function() {
        if (this.hasPrev()) {
            // Returns the prev index
            return parseInt(this.current) - 1;
        } else {
            return (circular) ? last : false;
        }
    };

    this.getNext = function() {
        if (this.hasNext()) {
            // Returns the next index
            return parseInt(this.current) + 1;
        } else {
            return (circular) ? 0 : false;
        }
    };

    this.getLast = function() {
        return last;
    };

    this.prev = function() {
        if (this.hasPrev()) {
            // Sets the current index with the previous index if it is not 0
            this.current--;
        } else {
            // Otherwise sets it with the last index
            if (circular) {
                this.last();
            }
        }

        // Returns the current index
        return this.current;
    };

    this.next = function() {
        if (this.hasNext()) {
            // Sets the current index with the next index if it is not greater than the last index
            this.current++;
        } else {
            // Otherwise sets it with the first index
            if (circular) {
                this.set(0);
            }
        }

        // Returns the current index
        return this.current;
    };

    this.last = function() {
        // Sets and returns the current index with the last index
        return (this.current = last);
    };
};


/*
 * Initiates the CCAINE object
 */
var CCAINE = CCAINE || {};


/*
 * Debugger Log
 * author: amontiel
 *
 * LOG:
 * Date: 25/04/14 - 11:24h
 * Date: 22/04/14 - 20:12h
 */
CCAINE.log = function(message, type) {
    type = type || 'log';

    if (CCAINE.log.debug) {
        console[type](message);
    }
};
CCAINE.log.debug = true;


/*
 * Performance Tester
 * author: amontiel
 *
 * LOG:
 * Date: 30/04/14 - 11:08h
 */
CCAINE.performance = function(callback) {
    var start, end;

    start = performance.now();

    callback();

    end = performance.now();

    CCAINE.log(end - start, 'debug');
};

/*
 * Slider
 * author: amontiel
 *
 * LOG:
 * Date: 29/04/14 - 00:24h
 */
CCAINE.slider = function() {
    var $slider = $('.slider'),
        $items = $slider.find('.slider-item'),
        itemsLength = $items.length,
        $btPrev = $slider.find('.slider-prev'),
        $btNext = $slider.find('.slider-next'),
        $bullets = $slider.find('.slider-bullets'),
        animating = false,
        duration = 1,
        delay = 5,
        timeout,
        pager;

    var methods = {
        enumerate: function() {
            $items.eq(0).addClass('current');

            for (var i = 0; i < itemsLength; i++) {
                $items.eq(i).attr('data-index', i);
            }
        },

        createBullets: function() {
            for (var i = 0; i < itemsLength; i++) {
                $bullets.append('<a class="slider-bullet" data-index="' + i + '" href="javascript:;"></a>');
            }
        },

        animate: function() {
            animating = true;

            TweenLite.from($slider.find('.current'), duration, {
                left: '100%'
            });
            TweenLite.to($slider.find('.prev'), duration, {
                left: '-100%',
                onComplete: function() {
                    animating = false;
                    $slider.find('.prev').css('left', 0);
                    methods.play();
                }
            });
        },

        set: function(i) {
            if (!animating && i !== pager.current) {
                pager.set(i);

                $items.removeClass('prev');

                $slider.find('.current').removeClass('current').addClass('prev');
                $slider.find('.slider-item[data-index="' + pager.current + '"]').addClass('current');

                methods.animate();
            }
        },

        prev: function() {
            methods.set(pager.getPrev());
        },

        next: function() {
            methods.set(pager.getNext());
        },

        bind: function() {
            $btPrev.on('click', function() {
                methods.prev();
            });

            $btNext.on('click', function() {
                methods.next();
            });

            $('.slider-bullet').on('click', function() {
                methods.set($(this).attr('data-index'));
            });
        },

        play: function() {
            window.clearTimeout(timeout);

            timeout = window.setTimeout(function() {
                methods.next();
                window.clearTimeout(timeout);
            }, delay * 1000);
        },

        init: function() {
            pager = new Paginator(itemsLength);

            methods.enumerate();
            methods.createBullets();
            methods.bind();
            methods.play();
        }
    };

    return ($slider.length) ? methods.init() : false;
};


/*
 * Display Items
 * author: rcruz
 *
 * LOG:
 * Date: 30/04/14 - 15:36h
 */
CCAINE.displayItems = function() {
    var $displayItem = $('.display-item');

    var methods = {
        bind: function() {
            $displayItem.on('click', function() {
                var $this = $(this);

                if ($this.hasClass('active')) {
                    $this.removeClass('active');
                } else {
                    $displayItem.removeClass('active');
                    $this.addClass('active');
                }
            });
        },

        init: function() {
            methods.bind();
        }
    };

    return ($displayItem.length) ? methods.init() : false;
};

/*
	Invible Shelf Control
	author: rcruz
*/
CCAINE.invisibleShelfControl = function() {

    var $invisibleShelf = $('.invisible-shelf'),
        $btShelf = $('.bt-shelf'),
        $shelfItem = $('.shelf-item');

    var methods = {
        bind: function() {
            $btShelf.on('click', function() {
                var $this = $(this),
                    $shelf = $this.parent().find('.shelf');
                if ($shelf.hasClass('invisible')) {

                    $('.shelf').addClass('invisible');
                    $shelf.removeClass('invisible');
                } else {

                    $('.shelf').addClass('invisible');
                }
            });

            $shelfItem.on('click', function() {
                var $this = $(this),
                    $displayDownload = $this.find('.display-download');

                if ($displayDownload.hasClass('active')) {
                    $displayDownload.removeClass('active');
                } else {
                    $('.display-download').removeClass('active');
                    $displayDownload.addClass('active');
                }
            });
        },

        init: function() {
            methods.bind();
        }
    };

    return ($shelfItem.length) ? methods.init() : false;
};

/*
 *   Invible Control Teaser
 *   author: rcruz
 *   LOG:
 *   Date: 06/05/14 - 17:00h
 */
CCAINE.controlTeaser = function() {

    var $coloredTeaser = $('.colored-teaser'),
        $mainTitle = $('.main-title'),
        $teaserDescription = $('.teaser-description'),

        $teaserInfo = $('.teaser-info'),
        $teaserMenuLink = $('.teaser-menu-link');


    var methods = {
        animate: function() {
            window.scrollTop = 0;
            TweenLite.to($coloredTeaser, 2, {
                marginTop: '-390px'
            });

            $mainTitle.css('margin-top', '30px');
            $teaserDescription.css('display', 'none');
            $teaserInfo.css('display', 'block');
        },

        bind: function() {
            $teaserMenuLink.on('click', function(e) {
                methods.animate();
            });
        },

        init: function() {
            methods.bind();

            if (window.location.hash !== '') {
                methods.animate();
            }

        }
    };
    return ($coloredTeaser.length) ? methods.init() : false;

};

window.onload = function(e) {
    //Home Page
    CCAINE.slider();
    //Team, Credentials
    CCAINE.displayItems();
    //Downloads//Profile
    CCAINE.invisibleShelfControl();
    //allTeasers
    CCAINE.controlTeaser();
};
